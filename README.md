$partenaire = ['persones' =>
      [['name' => 'Ivan', 'age' => 45, 'country' => 'RU', 'role' => 2, 'name_2' =>''],
       ['name' => 'Oleg', 'age' => 20, 'country' => 'RU', 'role' => 2, 'name_2' =>''],
       ['name' => 'Yaroslav', 'age' => 25, 'country' => 'DE'],
      ],
    ];

    if (!empty($partenaire)) {
      $person_val = [];
      foreach ($partenaire['persones'] as $person) {
        $person_val_field = [];
        if (!empty($person['role']) && $person['role'] !== 2 || empty($person['role'])) {
          continue;
        }
        $boom = 44;
        foreach ($person as $field_name => $value) {
          if (!empty($value)) {
            $person_val_field[$field_name] = $value;
          }
        }
        if (!empty($person_val_field)){
          $person_val[] = $person_val_field;
        }
      }
      $partenaire['persones'] = $person_val;
      $boom = 33;
    }
